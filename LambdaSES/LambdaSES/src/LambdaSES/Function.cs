using Amazon;
using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace LambdaSES;

public class Function
{
    public async Task FunctionHandler(SQSEvent inputEvent, ILambdaContext context)
    {
        foreach (var record in inputEvent.Records)
        {
            var emailSettings = new EmailSettings();

            emailSettings.senderAddress = record.MessageAttributes["senderAddress"].StringValue;
            emailSettings.receiverAddress = record.MessageAttributes["receiverAddress"].StringValue;
            
            using (var client = new AmazonSimpleEmailServiceClient(RegionEndpoint.EUWest1))
            {
                var sendRequest = new SendEmailRequest
                {
                    Source = emailSettings.senderAddress,
                    Destination = new Destination
                    {
                        ToAddresses =
                            new List<string> { emailSettings.receiverAddress }
                    },
                    Message = new Message
                    {
                        Subject = new Content("Amazon SES test email"),
                        Body = new Body
                        {
                            Text = new Content
                            {
                                Charset = "UTF-8",
                                Data = "This email was sent through Amazon SES using the AWS SDK for .NET."
                            }
                        }
                    }
                };
            
                try
                {
                    Console.WriteLine("Sending email using Amazon SES...");
                    var response = await client.SendEmailAsync(sendRequest);
                    Console.WriteLine("The email was sent successfully.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("The email was not sent.");
                    Console.WriteLine("Error message: " + ex.Message);
        
                }
            }
        }
    }
}
