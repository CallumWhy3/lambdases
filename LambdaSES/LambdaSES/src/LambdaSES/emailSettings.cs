namespace LambdaSES;

public class EmailSettings
{
    public string senderAddress { get; set; }
    public string receiverAddress { get; set; }
}